DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

/* CUSTOM TABLES */
CREATE TABLE `inventory` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `userid`  INT(11) NOT NULL,
  `cardid`  INT(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `hp` varchar(45) DEFAULT NULL,
  `attack` varchar(45) DEFAULT NULL,
  `defence` varchar(45) DEFAULT NULL,
  `energy` varchar(45) DEFAULT NULL,
  `imgurl` varchar(300) DEFAULT NULL,
  `price` INTEGER DEFAULT 50,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



DROP TABLE IF EXISTS `user`;

/* CUSTOM TABLES */
CREATE TABLE `user` (
    `id` INT(11) NOT NULL AUTO_INCREMENT ,
    `email` VARCHAR(50) NOT NULL ,
    `firstname` VARCHAR(50) NOT NULL ,
    `lastname` VARCHAR(50) NOT NULL ,
    `password` VARCHAR(100) NOT NULL ,
	`money` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE `USER_EMAIL_INDEX` (`email`)
) ENGINE = InnoDB;

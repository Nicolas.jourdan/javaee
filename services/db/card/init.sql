DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `family` varchar(45) DEFAULT NULL,
  `hp` varchar(45) DEFAULT NULL,
  `attack` varchar(45) DEFAULT NULL,
  `defence` varchar(45) DEFAULT NULL,
  `energy` varchar(45) DEFAULT NULL,
  `imgurl` varchar(300) DEFAULT NULL,
  `price` INTEGER DEFAULT 50,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `card`(`name`, `description`, `family`, `hp`, `attack`, `defence`, `energy`, `imgurl`) VALUES
('Bulbizarre','bulbi','Plante','60','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png'),
('Herbizarre','herbi','Plante','80','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png'),
('Florizarre','flori','Plante','140','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/003.png'),

('Salamèche','salam','Feu','60','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/004.png'),
('Reptincel','reptin','Feu','80','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/005.png'),
('Dracaufeu','dracau','Feu','140','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png'),

('Carapuce','cara','Eau','60','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/007.png'),
('Carabaffe','caraba','Eau','80','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/008.png'),
('Tortank','tortank','Eau','140','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/009.png'),

('Pichu','pika','electrique','60','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/172.png'),
('Pikachu','pikaaaa','electrique','80','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/025.png'),
('Raichu','pikachu','electrique','140','1','1','1','https://assets.pokemon.com/assets/cms2/img/pokedex/detail/026.png');


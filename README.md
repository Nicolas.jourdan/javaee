# JavaEE
## Gitlab : https://gitlab.com/Nicolas.jourdan/javaee
## YouTube video : https://youtu.be/5tqwMcINdqg
## Contents
* [Presentation](#presentation)
  * [Team](#team)
  * [Achievement](#achievement)
* [Set up the project](#set-up-the-project)
  * [Requirements](#requirements)
  * [Clone the project](#clone-the-project)
  * [Launch](#launch)
* [How to use the API ?](#how-to-use-the-api)
  * [Micro-services](#micro-services)
  * [Architecture](#architecture)
    * [Microservices and SOA (Service-Oriented Architecture)](#microservices-and-soa-service-oriented-architecture)
  * [Tests](#tests)
    * [Sonarqube](#sonarqube)
    * [Test plan](#test-plan)
      * [Inventory tests](#inventory-tests)
      * [Card tests](#card-tests)
      * [User tests](#user-tests)
      * [Authentication tests](#authentication-tests)
      * [Shop tests](#shop-tests)
      * [Room tests](#room-tests)
      * [FrontProxy tests](#frontproxy-tests)
  * [Authentication](#authentication)
  * [Resources](#resources)
    * [Card](#card)
      * [Get all cards](#get-all-cards)
      * [Get random cards](#get-random-cards)
      * [Get a specific card](#get-a-specific-card)
      * [Delete a specific card](#delete-a-specific-card)
      * [Create a card](#create-a-card)
      * [Update a card](#update-a-card)
      * [Delete all cards](#delete-all-cards)
    * [User](#user)
      * [Get all users](#get-all-users)
      * [Get a specific user](#get-a-specific-user)
      * [Get a user by email](#get-a-user-by-email)
      * [Delete a specific user](#delete-a-specific-user)
      * [Create a user](#create-a-user)
      * [Update a card](#update-a-user)
      * [Delete all users](#delete-all-users)
      * [Generate password](#generate-password)
    * [Inventory](#inventory)
      * [Get all inventories](#get-inventory)
      * [Get a specific inventory](#get-a-specific-inventory)
      * [Delete a specific inventory](#delete-a-specific-inventory)
      * [Add a card to a user](#add-a-card-to-a-user)
      * [Delete all inventories](#delete-all-inventories)

## Presentation
### Team
- ASSAF Caroline
- DELANNAY Théotil
- GRAS Marianne
- JOURDAN Nicolas
### Achievement
- API RESTFull ([api documentation](#how-to-use-the-api))
- Web application
  - Registration
  - Login
  - Authentication
  - Send cards
  - Buy cards
  - Logger
  - Docker
  - Sonarqube
  - Tests
## Set up the project

### Requirements

```
- docker  
- docker-compose  
- git  
```

### Clone the project

```
git clone https://gitlab.com/Nicolas.jourdan/javaee.git
```

### Launch
*You have to run docker before running docker-compose.*
```
# your_path/javaee/  

docker-compose build  
docker-compose up -d
```

## How to use the API

You can send some requests with Postman to test this API.
## Micro-services
This application is devided in several micro-services.  

| Micro-service | Host          | Port | Description  |
| :------------- |:-------------:| :-----:|:----:|
|  Proxy & Front | http://localhost | 8080 |     Proxy management     |
|      User      | http://localhost | 8086 |      User management     |
|      Card      | http://localhost | 8087 |      Card management     |
|    Inventory   | http://localhost | 8088 |   Inventory management   |
| Authentication | http://localhost | 8089 | Login and authentication |
|      Shop      | http://localhost | 8090 |      Shop management     |

*The proxy and the frond are on the same application*

## Architecture
![](images/architecture.png)

In order to implement this architecture we used **Docker** and **docker-compose**. Each microservice is inside a container.  
Moreover we separated databases from their microservice. For exemaple the user database and the UserService are not in the same container.  

### Microservices and SOA (Service-Oriented Architecture)

Even though the microservices architecture is based on the SOA, it brings so many advantages that we could compare them to a nowadays’s car and one from the 30’.

SOA improved the way of thinking applications: we no longer see them as just one bloc. Which caused applications to run everything and maked them hard to maintain or improve.

So with SOA, parts of the application are separated and easier to modify. Also when already in production, the application doesn’t need to be deployed in its entirety. But as it separated the code into big features, each service in the application depends on the others to run. If one of the service is down, the full application is unreachable.

Hence the microservices architecture, where each service is fully independent from the others, letting the application up even if one or more of the services are down. The others services, if they need ressources from the down service, won’t stop their execution and change into degraded mode.

Of course this impact directly the time of response. Because SOA applications have a middleware to make each services communicate with others, this time is rather short. On the other hand, if a service needs datas from another, which also needs another service: the time of response will increase with the number of request between services.

![](images/microservices-scheme.png)

## Tests

### Sonarqube

We used **Sonarqube** for continuous integration. Thanks to this application we shawn test coverage, bugs, vulnerabilities and some other important informations.  

![](images/sonar.png)

### Test plan
*A lot of Dto are not tested, this is the reason of the low test coverage of some service like FrontProxyService*

#### Inventory tests
![](images/inventory.png)

#### Card tests
![](images/card.png)

#### User tests
![](images/user.png)

#### Authentication tests
![](images/authentication.png)

#### Shop tests
![](images/shop.png)

#### Room tests
![](images/room.png)

#### FrontProxy tests
![](images/frontproxy.png)

## Authentication

### Endpoint
| Endpoint        | Method           | Description  | Header |
| :------------- |:-------------:| :-----:| :---: |
| http://localhost:8089/login      | POST | Authenticate | application/json |
| http://localhost:8089/checkAuthentication      | POST | Check user Authentication | application/json |

## Login
#### Request

**Body**

```json
{
	"email":"first@test.com",
	"password": "password"
}
```

#### Responses

*Authenticated*

**HTTP Response Code** : 200

Token and user id

**Body**

```json
{
    "token": "1b144dba4bf4f68defc9aefce595b7a2828b38e5f1d111f085009a604dca6137"
    "userId":"1"
}
```

The token have to be save in a cookie named **__api_token** and the userId in a cookie named **__api_user_id**.  
These cookies will be checked by the API to authenticate the user.

*Unauthorized*

**HTTP Response Code** : 401

**Body**

```
No content
```

## Check authentication
#### Request

**Body**

```json
{
    "token": "1b144dba4bf4f68defc9aefce595b7a2828b38e5f1d111f085009a604dca6137"
    "userId":"1"
}
```

#### Responses

*Authenticated*

**HTTP Response Code** : 200

**Body**

```
No content
```

*Unauthorized or Conflict*

**HTTP Response Code** : 409

**Body**

```
No content
```

## Resources

## Card

### Endpoints
| Endpoint        | Method           | Description  | Header |
| :------------- |:-------------:| :-----:| :---: |
| http://localhost:8087/cards      | GET | Get all cards | - |
| http://localhost:8087/cards/random/{nb}      | GET | Get {nb} random cards | - |
| http://localhost:8087/cards/{id}      | GET | Get a specific card | - |
| http://localhost:8087/cards/{id}      | DELETE | Delete a specific card | - |
| http://localhost:8087/cards      | POST      | Create a card | application/json |
| http://localhost:8087/cards/{id}      | PUT      | Update a card | application/json |
| http://localhost:8087/cards      | DELETE      | Delete all cards | - |

### Get all cards
#### Responses

*Found*

**HTTP Response Code** : 200

Array of cards

**Body**
```json
[
    {
        "id": 1,
        "name": "Bulbizarre",
        "description": "bulbi",
        "family": "Plante",
        "hp": "60",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
        "price": 50
    },
    {
        "id": 2,
        "name": "Herbizarre",
        "description": "herbi",
        "family": "Plante",
        "hp": "80",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png",
        "price": 50
    }
]
```

### Get random cards

This route returns 5 cards by default if {nb} is badly formatted.  

{nb} should be between 3 and 10.

#### Responses

*Found*

**HTTP Response Code** : 200

Array of cards

**Body**
```json
[
    {
        "id": 1,
        "name": "Bulbizarre",
        "description": "bulbi",
        "family": "Plante",
        "hp": "60",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
        "price": 50
    },
    {
        "id": 2,
        "name": "Herbizarre",
        "description": "herbi",
        "family": "Plante",
        "hp": "80",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png",
        "price": 50
    },
    {
        "id": 8,
        "name": "Carabaffe",
        "description": "caraba",
        "family": "Eau",
        "hp": "80",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/008.png",
        "price": 50
    }
]
```

### Get a specific card
#### Responses

*Found*

**HTTP Response Code** : 200

The card 

**Body**
```json
{
    "id": 1,
    "name": "Bulbizarre",
    "description": "bulbi",
    "family": "Plante",
    "hp": "60",
    "attack": "1",
    "defence": "1",
    "energy": "1",
    "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
    "price": 50
}
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```

### Delete a specific card
#### Responses

*Found*

**HTTP Response Code** : 204

**Body**
```
No content
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```

### Create a card
### Request
**Body**

```json
{
	"name":"Simiabraz",
	"description":"Best pokemon ever",
	"family":"feu",
	"hp":"80",
	"attack":"20",
	"defence":"20",
	"energy":"20",
	"imgurl":"https://assets.pokemon.com/assets/cms2/img/pokedex/detail/392.png"
}
```
#### Responses

*Created*

**HTTP Response Code** : 201

**Body**

The created card
```json
{
	"name":"Simiabraz",
	"description":"Best pokemon ever",
	"family":"feu",
	"hp":"80",
	"attack":"20",
	"defence":"20",
	"energy":"20",
	"imgurl":"https://assets.pokemon.com/assets/cms2/img/pokedex/detail/392.png"
}
```

### Update a card
#### Request

**Body**

```json
{
	"name":"Simiabraz",
	"description":"Best pokemon ever",
	"family":"feu",
	"hp":"80",
	"attack":"20",
	"defence":"20",
	"energy":"20",
	"imgurl":"https://assets.pokemon.com/assets/cms2/img/pokedex/detail/392.png"
}
```

#### Responses

*Updated*

**HTTP Response Code** : 200

**Body**

The updated card
```json
{
	"name":"Simiabraz",
	"description":"Best pokemon ever",
	"family":"feu",
	"hp":"80",
	"attack":"20",
	"defence":"20",
	"energy":"20",
	"imgurl":"https://assets.pokemon.com/assets/cms2/img/pokedex/detail/392.png"
}
```

### Delete all cards
#### Responses

*Deleted*

**HTTP Response Code** : 204

**Body**
```
No content
```

## User

### Endpoints
| Endpoint        | Method           | Description  | Header |
| :------------- |:-------------:| :-----:| :---: |
| http://localhost:8086/users      | GET | Get all users | - |
| http://localhost:8086/users/{id}      | GET | Get a specific user | - |
| http://localhost:8086/users/email      | POST      | Get a user by email | application/json |
| http://localhost:8086/users/{id}      | DELETE | Delete a specific user | - |
| http://localhost:8086/users      | POST      | Create a user | application/json |
| http://localhost:8086/users/{id}      | PUT      | Update a user | application/json |
| http://localhost:8086/users      | DELETE      | Delete all users | - |
| http://localhost:8086/users/password | POST      | Hash a password | application/json |


### Get all users
#### Responses

*Found*

**HTTP Response Code** : 200

Array of users

**Body**
```json
[
    {
        "id": 1,
        "email": "first@test.com",
        "firstname": "myName",
        "lastname": "myLastName",
        "password": "7d93acdsdsdsdsd339549e59bc9392dddd21bf9f7987b2dc6fe9d76fa22ee688",
        "money": 5000
    },
    {
        "id": 2,
        "email": "second@test.com",
        "firstname": "myName",
        "lastname": "myLastName",
        "password": "7d93ac8c0bd95cd339549e59bc9392dddd21bf9f7987b2dc6fe9d76fa22ee688",
        "money": 5000
    }
]
```

### Get a specific user
#### Responses

*Found*

**HTTP Response Code** : 200

The user

**Body**
```json
{
    "id": 1,
    "email": "first@test.com",
    "firstname": "myName",
    "lastname": "myLastName",
    "password": "7d93acdsdsdsdsd339549e59bc9392dddd21bf9f7987b2dc6fe9d76fa22ee688",
    "money": 5000
}
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```
### Get a user by email
#### Request

**Body**

```json
{
  "email":"first@test.com"
}
```

#### Responses

*Found*

**HTTP Response Code** : 200

The user

**Body**
```json
{
    "id": 1,
    "email": "first@test.com",
    "firstname": "myName",
    "lastname": "myLastName",
    "password": "7d93acdsdsdsdsd339549e59bc9392dddd21bf9f7987b2dc6fe9d76fa22ee688",
    "money": 5000
}
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```

### Delete a specific user
#### Responses

*Found*

**HTTP Response Code** : 204

**Body**
```
No content
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```

### Create a user
#### Request

**Body**

```json
{
	"email":"first@test.com",
	"firstname":"myName",
	"lastname":"myLastName",
	"password":"password"
}
```

#### Responses

*Created*

**HTTP Response Code** : 201

**Body**

The created user
```json
{
    "id": 1,
	"email":"first@test.com",
	"firstname":"myName",
	"lastname":"myLastName",
	"password":"7d93acdsdsdsdsd339549e59bc9392dddd21bf9f7987b2dc6fe9d76fa22ee688",
	"money": 5000
}
```

### Update a user
#### Request

**Body**

```json
{
	"email":"first@test.com",
	"firstname":"myName",
	"lastname":"myLastName",
	"password":"newPassword"
}
```

#### Responses

*Updated*

**HTTP Response Code** : 200

**Body**

The updated user
```json
{
    "id": 1,
	"email":"first@test.com",
	"firstname":"myName",
	"lastname":"myLastName",
	"password":"7d93acdsdsdspdkfd9549e59bc9392dddd21bf9f7987b2dc6fe9d76fa22ee688",
	"money": 5000
}
```

### Delete all users
#### Responses

*Deleted*

**HTTP Response Code** : 204

**Body**
```
No content
```
### Generate password
#### Request

**Body**

```json
{
  "password":"password_to_hash"
}
```

#### Responses

*Hashed*

**HTTP Response Code** : 200

The hashed password

**Body**
```json
{
    "password": "0f54109ef1cd1f3ae1ccb8822a5d0be3803edac3604a8af787c852a187613515"
}
```

## Inventory

### Endpoints
| Endpoint        | Method           | Description  | Header |
| :------------- |:-------------:| :-----:| :---: |
| http://localhost:8088/inventory/{userid}      | GET | Get user cards | - |
| http://localhost:8088/inventory/{userid}      | POST | Add cards to a user | - |
| http://localhost:8088/inventory/{userid}/{cardid}    | GET | Get a specific user card | - |
| http://localhost:8088/inventory/{userid}/{cardid}    | DELETE | Delete a specific user card | - |
| http://localhost:8088/inventory/cards/{userid}      | POST | Add a card to a user | - |

### Get users cards
#### Responses

*Found*

**HTTP Response Code** : 200

Array of card

**Body**
```json
[
    {
        "id": 1,
        "name": "Bulbizarre",
        "description": "bulbi",
        "family": "Plante",
        "hp": "60",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
        "price": 50
    },
    {
        "id": 2,
        "name": "Herbizarre",
        "description": "herbi",
        "family": "Plante",
        "hp": "80",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png",
        "price": 50
    }
]
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```

### Add cards to a user
#### Request

**Body**

```json
[
    {
        "id": 1,
        "name": "Bulbizarre",
        "description": "bulbi",
        "family": "Plante",
        "hp": "60",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
        "price": 50
    },
    {
        "id": 2,
        "name": "Herbizarre",
        "description": "herbi",
        "family": "Plante",
        "hp": "80",
        "attack": "1",
        "defence": "1",
        "energy": "1",
        "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png",
        "price": 50
    }
]
```
#### Responses

*Added*

**HTTP Response Code** : 201

**Body**

```
No content
```

*User not found or User already has the card*

**HTTP Response Code** : 409

**Body**

```
No content
```

### Get a specific user card
#### Responses

*Found*

**HTTP Response Code** : 200

The user card

**Body**
```json
{
    "id": 1,
    "name": "Bulbizarre",
    "description": "bulbi",
    "family": "Plante",
    "hp": "60",
    "attack": "1",
    "defence": "1",
    "energy": "1",
    "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
    "price": 50
}
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```

### Delete a specific user card
#### Responses

*Found*

**HTTP Response Code** : 204

**Body**
```
No content
```

*Not found*

**HTTP Response Code** : 404

**Body**

```
No content
```

### Add a card to a user
#### Request

**Body**

```json
{
    "id": 1,
    "name": "Bulbizarre",
    "description": "bulbi",
    "family": "Plante",
    "hp": "60",
    "attack": "1",
    "defence": "1",
    "energy": "1",
    "imgurl": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
    "price": 50
}
```
#### Responses

*Added*

**HTTP Response Code** : 201

**Body**

```
No content
```

*User not found or User already has the card*

**HTTP Response Code** : 409

**Body**

```
No content
```

package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestCardApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestCardApplication.class, args);
	}
}

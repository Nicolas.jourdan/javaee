package com.cpe.springboot.card.repository;

import com.cpe.springboot.card.model.Card;
import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<Card, Integer> {
}

package com.cpe.springboot.card.service;


import com.cpe.springboot.card.model.Card;
import com.cpe.springboot.card.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

@Service
public class CardService {
	
    private static final int MAX_CARD_NUMBER = 7;
	private static final int MIN_CARD_NUMBER = 3;

	@Autowired
    private CardRepository cardRepository;

    public List<Card> getAllCards() {
        List<Card> cards = new ArrayList<>();
        this.cardRepository.findAll().forEach(cards::add);
        return cards;
    }
    
    public List<Card> getRandomCards(String nb) {
    	List<Card> result = new ArrayList<>();
        List<Card> cards = this.getAllCards();
    	int cardsNumber;
        
        //check if nb is an int
        try {
        	cardsNumber = Integer.parseInt(nb);
    	}
    	catch(Exception e) {
    		cardsNumber = MIN_CARD_NUMBER;
    	}
    	
        //check if nb is between min and max, if not, set it to max.
        if(cardsNumber < MIN_CARD_NUMBER || cardsNumber > MAX_CARD_NUMBER) {
        	cardsNumber = MIN_CARD_NUMBER;
        }
        
        for (int i = 0; i < cardsNumber; i++) {
            int randomIndex = new Random().nextInt(cards.size());
            Card card = cards.get(randomIndex);
            result.add(card);
            cards.remove(card);
        }

        return result;
    }

    public Card getCard(String id) {
        return this.cardRepository.findOne(Integer.valueOf(id));
    }

    public void addCard(Card card) {
        this.cardRepository.save(card);
    }

    public void updateCard(Card card) {
        this.cardRepository.save(card);

    }

    public boolean deleteCard(String id) {
        Card cardToDelete = this.cardRepository.findOne(Integer.parseInt(id));

        if (Objects.isNull(cardToDelete)) {
            return false;
        }

        this.cardRepository.delete(Integer.valueOf(id));

        return true;
    }

    public void deleteCards() {
        this.cardRepository.deleteAll();
    }
}

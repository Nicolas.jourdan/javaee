package com.cpe.springboot.card.controller;

import com.cpe.springboot.card.model.Card;
import com.cpe.springboot.card.service.CardService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class CardRestController {

    private static final Logger logger = Logger.getLogger(CardRestController.class);

    @Autowired
    private CardService cardService;

    /**
     * Return all the existing cards
     */
    @GetMapping("/cards")
    public ResponseEntity<List<Card>> getCards() {
        logger.info("GET on /cards");
        return new ResponseEntity<>(this.cardService.getAllCards(), HttpStatus.OK);
    }

    /**
     * Return {nb} random cards
     */
    @GetMapping("/cards/random/{nb}")
    public ResponseEntity<List<Card>> getRandomCards(@PathVariable String nb) {
        logger.info("GET on /cards/random/" + nb);

        return new ResponseEntity<>(this.cardService.getRandomCards(nb), HttpStatus.OK);
    }

    /**
     * Return the card with the id {id}
     */
    @GetMapping("/cards/{id}")
    public ResponseEntity<Card> getCard(@PathVariable String id) {
        logger.info("GET on /cards/" + id);
        Card card = this.cardService.getCard(id);

        if (Objects.isNull(card)) {
            logger.info("NOT FOUND");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        logger.info("FOUND card with id " + card.getId());
        return new ResponseEntity<>(card, HttpStatus.OK);
    }

    /**
     * Add a new card to the list of the existing cards
     */
    @PostMapping("/cards")
    public ResponseEntity<Card> addCard(@RequestBody Card card) {
        logger.info("POST on /cards");

        this.cardService.addCard(card);

        return new ResponseEntity<>(card, HttpStatus.CREATED);
    }

    /**
     * Update the card with the id {id} with the parameter of the card (from request_body)
     */
    @PutMapping("/cards/{id}")
    public ResponseEntity<Card> updateCard(@RequestBody Card card,@PathVariable String id) {
        logger.info("PUT on /cards/" + id);

        Card cardInDatabase = this.cardService.getCard(id);

        if (Objects.isNull(cardInDatabase)) {
            logger.info("NOT FOUND");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        card.setId(Integer.valueOf(id));
        this.cardService.updateCard(card);

        logger.info("FOUND card with id " + card.getId());

        return new ResponseEntity<>(card, HttpStatus.OK);
    }

    /**
     * Delete a card
     * @param id card id
     */
    @DeleteMapping("/cards/{id}")
    public ResponseEntity deleteCard(@PathVariable String id) {
        logger.info("DELETE on /cards/" + id);

        if (this.cardService.deleteCard(id)) {
            logger.info("DELETED card with id " + id);

            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        logger.info("NOT FOUND");

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    /**
     * Delete all cards
     */
    @DeleteMapping("/cards")
    public ResponseEntity deleteCards() {
        logger.info("DELETE on /cards");
        this.cardService.deleteCards();

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}

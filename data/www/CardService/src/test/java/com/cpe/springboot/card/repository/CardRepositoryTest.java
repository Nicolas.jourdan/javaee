package com.cpe.springboot.card.repository;

import com.cpe.springboot.card.model.Card;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CardRepositoryTest {

    @Autowired
    private CardRepository cardRepository;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll()
    {
        List<Card> cards = new ArrayList<>();
        cards.add(new Card("Simiabrazz", "desc", "feu", "80", "20", "20", "20", "url", 50));
        cards.add(new Card("Pikachuu", "desc", "feu", "80", "20", "20", "20", "url", 50));

        this.cardRepository.save(cards.get(0));
        this.cardRepository.save(cards.get(1));

        Iterable<Card> iterableExpected = new ArrayList<>(cards);

        assertEquals(iterableExpected, this.cardRepository.findAll());
    }
}

package com.cpe.springboot.card.controller;

import com.cpe.springboot.card.model.Card;
import com.cpe.springboot.card.service.CardService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CardRestController.class, secure = false)
public class CardRestControllerTest {

    @InjectMocks
    private CardRestController cardRestController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CardService cardService;

    private Card card;

    private List<Card> cards;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        this.card = new Card("Simiabrazz", "desc", "feu", "80", "20", "20", "20", "url", 50);
        this.card.setId(1);

        this.cards = new ArrayList<>();
        this.cards.add(new Card("Simiabraz", "desc", "feu", "80", "20", "20", "20", "url", 50));
        this.cards.add(new Card("Pikachu", "desc", "feu", "80", "20", "20", "20", "url", 50));
        this.cards.add(new Card("Pichu", "desc", "feu", "80", "20", "20", "20", "url", 50));
        this.cards.add(new Card("Ponita", "desc", "feu", "80", "20", "20", "20", "url", 50));
        this.cards.get(0).setId(1);
        this.cards.get(1).setId(2);
        this.cards.get(2).setId(3);
        this.cards.get(3).setId(4);
    }

    @Test
    public void testGetRandomCards() throws Exception {
        when(this.cardService.getRandomCards(anyString())).thenReturn(this.cards);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cards/random/4").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "[{\"id\":1,\"name\":\"Simiabraz\",\"description\":\"desc\",\"family\":\"feu\",\"hp\":\"80\",\"attack\":\"20\","
                + "\"defence\":\"20\",\"energy\":\"20\",\"imgurl\":\"url\",\"price\":50},{\"id\":2,\"name\":\"Pikachu\","
                +"\"description\":\"desc\",\"family\":\"feu\",\"hp\":\"80\",\"attack\":\"20\",\"defence\":\"20\",\"energy\":\"20\","
                +"\"imgurl\":\"url\",\"price\":50},{\"id\":3,\"name\":\"Pichu\",\"description\":\"desc\",\"family\":\"feu\",\"hp\":\"80\","
                +"\"attack\":\"20\",\"defence\":\"20\",\"energy\":\"20\",\"imgurl\":\"url\",\"price\":50},{\"id\":4,\"name\":\"Ponita\","
                +"\"description\":\"desc\",\"family\":\"feu\",\"hp\":\"80\",\"attack\":\"20\",\"defence\":\"20\",\"energy\":\"20\","
                +"\"imgurl\":\"url\",\"price\":50}]",
                result.getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void testGetUser() throws Exception {
        when(this.cardService.getCard(anyString())).thenReturn(this.card);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cards/1").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "{\"id\":1,\"name\":\"Simiabrazz\",\"description\":\"desc\",\"family\":\"feu\",\"hp\":\"80\",\"attack\":\"20\",\"defence\":\"20\",\"energy\":\"20\",\"imgurl\":\"url\",\"price\":50}",
                result.getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void testGetCards() throws Exception {
        when(this.cardService.getAllCards()).thenReturn(Arrays.asList(this.card));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cards").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "[{\"id\":1,\"name\":\"Simiabrazz\",\"description\":\"desc\",\"family\":\"feu\",\"hp\":\"80\",\"attack\":\"20\",\"defence\":\"20\",\"energy\":\"20\",\"imgurl\":\"url\",\"price\":50}]",
                result.getResponse().getContentAsString(),
                false
        );
    }
}

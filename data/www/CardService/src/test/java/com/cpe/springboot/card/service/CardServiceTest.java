package com.cpe.springboot.card.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.cpe.springboot.card.model.Card;
import com.cpe.springboot.card.repository.CardRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@DataJpaTest
public class CardServiceTest {

    @InjectMocks
    private CardService cardService;

    @Mock
    private CardRepository cardRepository;

    private List<Card> cards;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        this.cards = new ArrayList<>();
        this.cards.add(new Card("Simiabraz", "desc", "feu", "80", "20", "20", "20", "url", 50));
        this.cards.add(new Card("Pikachu", "desc", "feu", "80", "20", "20", "20", "url", 50));
        this.cards.add(new Card("Pichu", "desc", "feu", "80", "20", "20", "20", "url", 50));
        this.cards.add(new Card("Ponita", "desc", "feu", "80", "20", "20", "20", "url", 50));

    }

    @Test
    public void testGetRandomCards()
    {
        // Mock findAll()
        Iterable<Card> cardIterable = new ArrayList<>(this.cards);
        when(this.cardRepository.findAll()).thenReturn(cardIterable);

        List<Card> cardsResult = this.cardService.getRandomCards("3");
        assertEquals(3, cardsResult.size());

        cardsResult = this.cardService.getRandomCards("4");
        assertEquals(4, cardsResult.size());
    }

    @Test
    public void testGetAllCards()
    {
        // Mock findAll()
        Iterable<Card> cardIterable = new ArrayList<>(this.cards);
        when(this.cardRepository.findAll()).thenReturn(cardIterable);

        // Mock expected card list
        Iterable<Card> cardIterableCopy = new ArrayList<>(this.cards);
        List<Card> expectedCards = new ArrayList<>();
        cardIterableCopy.forEach(expectedCards::add);

        // Get all cards
        List<Card> cards = this.cardService.getAllCards();

        assertEquals(expectedCards, cards);
    }
}

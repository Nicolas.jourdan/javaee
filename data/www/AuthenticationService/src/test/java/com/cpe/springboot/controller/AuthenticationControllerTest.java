package com.cpe.springboot.controller;

import com.cpe.springboot.authentication.controller.AuthenticationController;
import com.cpe.springboot.authentication.service.AuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AuthenticationController.class, secure = false)
public class AuthenticationControllerTest {

    @InjectMocks
    private AuthenticationController authenticationController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthenticationService authenticationService;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLogin() throws Exception {
        when(this.authenticationService.checkConnection(anyString(), anyString())).thenReturn(1);
        when(this.authenticationService.getGeneratePassword(any())).thenReturn("token");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"email\":\"email\",\"password\": \"password\"}");

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "{\"token\":\"token\",\"userId\":\"1\"}",
                result.getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void testCheckAuthentication() throws Exception {
        when(this.authenticationService.checkToken(anyString(), anyString())).thenReturn(true);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/checkAuthentication")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"token\":\"token\",\"userId\":\"1\"}");

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }
}

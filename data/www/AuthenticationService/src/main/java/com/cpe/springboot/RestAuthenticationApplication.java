package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestAuthenticationApplication.class, args);
	}
}

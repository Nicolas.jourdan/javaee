package com.cpe.springboot.authentication.service;

import com.cpe.springboot.authentication.model.CookieDto;
import com.cpe.springboot.authentication.model.PasswordDto;
import com.cpe.springboot.authentication.model.UserDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
public class AuthenticationService {
    private static final String LOCALHOST = "http://user";
    private static final int USER_SERVICE_PORT = 80;
    private static final String USER_SERVICE_GET_BY_EMAIL = "/users/email";
    private static final String USER_SERVICE_GET_HASH_PASSWORD = "/users/password";
    private static final String USER_SERVICE_GET_USER = "/users/";

    public static final int NOT_FOUND_OR_NOT_SAME_PASSWORD = -1;

    public int checkConnection(String email, String password) {
        UserDto userDto;

        try {
            userDto = this.getUserByEmail(email);
        } catch (Exception e) {
            // User not found
            return NOT_FOUND_OR_NOT_SAME_PASSWORD;
        }

        if (Objects.isNull(userDto)) {
            return NOT_FOUND_OR_NOT_SAME_PASSWORD;
        }

        // Hash password send by the client to compare with the user password
        String generatePassword = this.getGeneratePassword(password);

        if (!userDto.getPassword().equals(generatePassword)) {
            return NOT_FOUND_OR_NOT_SAME_PASSWORD;
        }

        return userDto.getId();
    }

    /**
     * Get a hashed password from the UserService
     *
     * @param password Password to hash
     *
     * @return "" -> Error
     */
    public String getGeneratePassword(String password) {
        ResponseEntity<PasswordDto> response;

        try {
            // Get hashed password
            String url = LOCALHOST + ":" + USER_SERVICE_PORT + USER_SERVICE_GET_HASH_PASSWORD;

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<String> request = new HttpEntity<String>(
                    "{\"password\":\"" + password + "\"}",
                    headers
            );

            RestTemplate restTemplate = new RestTemplate();
            response = restTemplate.postForEntity(url, request, PasswordDto.class);
        } catch (Exception e) {
            // Error during request generation
            return "";
        }

        return Objects.isNull(response) ? "" : response.getBody().getPassword();
    }

    public boolean checkToken(String cookieToken, String cookieUserId) {
        UserDto user;
        try {
            user = this.getUser(cookieUserId);
        } catch (Exception e) {
            return false;
        }

        if (Objects.isNull(user)) {
            return false;
        }

        // Create new cookieDto to compare with the cookieDto send by the client
        CookieDto cookieDto = new CookieDto(this.getGeneratePassword(user.getEmail()), cookieUserId);

        return cookieDto.getToken().equals(cookieToken);
    }

    /**
     * Call UserService to get a User
     *
     * @param userId : User id
     *
     * @return UserDto related to the User or null if not found
     */
    private UserDto getUser(String userId) {
        ResponseEntity<UserDto> response;

        // Get hashed password
        String url = LOCALHOST + ":" + USER_SERVICE_PORT + USER_SERVICE_GET_USER + userId;

        RestTemplate restTemplate = new RestTemplate();

        response = restTemplate.getForEntity(url, UserDto.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        return null;
    }

    private UserDto getUserByEmail(String email) {
        ResponseEntity<UserDto> response;

        // Get user ID from email
        String url = LOCALHOST + ":" + USER_SERVICE_PORT + USER_SERVICE_GET_BY_EMAIL;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<String>(
                "{\"email\":\"" + email + "\"}",
                headers
        );

        RestTemplate restTemplate = new RestTemplate();

        response = restTemplate.postForEntity(url, request, UserDto.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        return null;
    }
}

package com.cpe.springboot.authentication.controller;

import com.cpe.springboot.authentication.service.AuthenticationService;
import com.cpe.springboot.authentication.model.LoginDto;
import com.cpe.springboot.authentication.model.CookieDto;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    private static final Logger logger = Logger.getLogger(AuthenticationController.class);

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginDto loginDto) {
        logger.info("POST on /login");
        int userId = this.authenticationService.checkConnection(loginDto.getEmail(), loginDto.getPassword());

        String token = this.authenticationService.getGeneratePassword(loginDto.getEmail());

        if (!(AuthenticationService.NOT_FOUND_OR_NOT_SAME_PASSWORD == userId) && !token.equals("")) {
            logger.info("AUTHORIZED");

            return new ResponseEntity(
                    new CookieDto(token, String.valueOf(userId)),
                    HttpStatus.OK
            );
        }

        logger.info("UNAUTHORIZED");

        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/checkAuthentication")
    public ResponseEntity checkAuthentication(@RequestBody CookieDto cookieDto) {
        logger.info("POST on /checkAuthentication");

        if (this.authenticationService.checkToken(cookieDto.getToken(), cookieDto.getUserId())) {
            logger.info("TOKEN is OK");
            return new ResponseEntity(HttpStatus.OK);
        }

        logger.info("TOKEN IS NOT OK");

        return new ResponseEntity(HttpStatus.CONFLICT);
    }
}

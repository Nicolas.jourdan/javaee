package com.cpe.springboot.user.controller;

import com.cpe.springboot.user.model.EmailDto;
import com.cpe.springboot.user.service.HasherService;
import com.cpe.springboot.user.model.PasswordDto;
import com.cpe.springboot.user.model.User;
import com.cpe.springboot.user.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class UserRestController {

    private static final Logger logger = Logger.getLogger(UserRestController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private HasherService hasherService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {
        logger.info("GET on /users");

        return new ResponseEntity<>(this.userService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity getUser(@PathVariable String id) {
        logger.info("GET on /users/" + id);

        User user = this.userService.getUser(id);

        if (Objects.isNull(user)) {
            logger.info("NOT FOUND");

            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        logger.info("FOUND");

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping("/users/email")
    public ResponseEntity getUserByEmail(@RequestBody EmailDto emailDto) {
        logger.info("GET on /users/email with body {\"email\":\"" + emailDto.getEmail() + "\"}");
        User user = this.userService.getUserByEmail(emailDto.getEmail());

        if (Objects.isNull(user)) {
            logger.info("NOT FOUND");

            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        logger.info("FOUND");

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<User> addUser(@RequestBody User user) {
        logger.info("POST on /users");

        if (this.userService.addUser(user)) {
            logger.info("CREATED");

            return new ResponseEntity<>(user, HttpStatus.CREATED);
        }

        logger.info("CONFLICT");

        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable String id) {
        logger.info("PUT on /users/" + id);
        User userInDatabase = this.userService.getUser(id);

        if (Objects.isNull(userInDatabase)) {
            logger.info("NOT FOUND");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        user.setId(Integer.valueOf(id));
        this.userService.updateUser(user);

        logger.info("FOUND");

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable String id) {
        logger.info("DELETE on /users/" + id);

        if (this.userService.deleteUser(id)) {
            logger.info("DELETED user with id " + id);

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        logger.info("NOT FOUND");

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users")
    public ResponseEntity deleteUsers() {
        logger.info("DELETE on /users");

        this.userService.deleteUsers();

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/users/password")
    public ResponseEntity<PasswordDto> getHashPassword(@RequestBody PasswordDto passwordDto) {
        logger.info("POST /users/password");

        return new ResponseEntity<>(new PasswordDto(this.hasherService.hash(passwordDto.getPassword())), HttpStatus.OK);
    }
}

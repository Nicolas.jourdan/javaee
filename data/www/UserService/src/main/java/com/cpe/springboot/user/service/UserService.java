package com.cpe.springboot.user.service;

import com.cpe.springboot.user.model.User;
import com.cpe.springboot.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private static final int DEFAULT_CARD_NUMBER = 5;
    private static final String CARD_HOSTNAME = "http://card";
    private static final int CARD_SERVICE_PORT = 80;
    private static final String CARD_SERVICE_RANDOM_CARDS = "/cards/random/";

    private static final String INVENTORY_HOSTNAME = "http://inventory";
    private static final int INVENTORY_SERVICE_PORT = 80;
    private static final String INVENTORY_SAVE = "/inventory/";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HasherService hasherService;

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        this.userRepository.findAll().forEach(users::add);
        return users;
    }

    public User getUser(String id) {
        return this.userRepository.findOne(Integer.valueOf(id));
    }

    public User getUserByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    public boolean addUser(User user) {

        User userInDatabase = this.userRepository.findByEmail(user.getEmail());
        if (!Objects.isNull(userInDatabase)) {
            return false;
        }

        if (0 == user.getMoney()) {
            user.setMoney(User.DEFAULT_MONEY);
        }

        user.setPassword(this.hasherService.hash(user.getPassword()));
        this.userRepository.save(user);

        try {
            this.addCardsToInventory(user.getId());
        } catch (Exception e) {
            // User created without inventory if CardService and/or InventoryService are down
        }

        return true;
    }


    public void updateUser(User user) {
        this.userRepository.save(user);
    }

    public boolean deleteUser(String id) {
        User user = this.getUser(id);

        if (Objects.isNull(user)) {
            return false;
        }

        this.userRepository.delete(Integer.valueOf(id));

        return true;
    }

    public void deleteUsers() {
        this.userRepository.deleteAll();
    }

    /**
     * Add random cards to the inventory
     *
     * @param userId : User id
     */
    private void addCardsToInventory(int userId) {
        // Get random cards
        String urlToGetCards = CARD_HOSTNAME + ":" + CARD_SERVICE_PORT + CARD_SERVICE_RANDOM_CARDS + DEFAULT_CARD_NUMBER;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseCards = restTemplate.getForEntity(urlToGetCards, String.class);

        // Save cards to the inventory
        String urlToSaveInventory = INVENTORY_HOSTNAME + ":" + INVENTORY_SERVICE_PORT + INVENTORY_SAVE + userId;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(responseCards.getBody(), headers);

        RestTemplate inventory = new RestTemplate();
        inventory.postForEntity(urlToSaveInventory, request, null);
    }
}

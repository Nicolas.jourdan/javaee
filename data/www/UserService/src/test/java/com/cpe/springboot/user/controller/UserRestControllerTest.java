package com.cpe.springboot.user.controller;

import com.cpe.springboot.user.model.User;
import com.cpe.springboot.user.service.HasherService;
import com.cpe.springboot.user.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserRestController.class, secure = false)
public class UserRestControllerTest {

    @InjectMocks
    private UserRestController userRestController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private HasherService hasherService;

    private User user;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        this.user = new User("email", "firstname", "lastname", "password");
        this.user.setId(1);
    }

    @Test
    public void testGetUser() throws Exception {
        when(this.userService.getUser("1")).thenReturn(this.user);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/1").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "{\"id\":1,\"email\":\"email\",\"firstname\":\"firstname\",\"lastname\":\"lastname\",\"password\":\"password\",\"money\":5000}",
                result.getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void testGetAllUsers() throws Exception {
        when(this.userService.getAllUsers()).thenReturn(Arrays.asList(this.user));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "[{\"id\":1,\"email\":\"email\",\"firstname\":\"firstname\",\"lastname\":\"lastname\",\"password\":\"password\",\"money\":5000}]",
                result.getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void testAddUser() throws Exception {
        when(this.userService.addUser(anyObject())).thenReturn(true);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":\"1\",\"email\":\"email\",\"firstname\":\"firstname\",\"lastname\":\"lastname\",\"password\":\"password\"}");

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "{\"id\":1,\"email\":\"email\",\"firstname\":\"firstname\",\"lastname\":\"lastname\",\"password\":\"password\",\"money\":5000}",
                result.getResponse().getContentAsString(),
                false
        );
    }
}

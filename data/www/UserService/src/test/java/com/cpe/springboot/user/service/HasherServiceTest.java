package com.cpe.springboot.user.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class HasherServiceTest {

    private static final String DEFAULT_HASH_FOR_EMPTY_PASSWORD = "6ee3d76f7ee5ab02cb8f70b011aa99331d76e33809951f052fb055fdf6338f46";

    @InjectMocks
    private HasherService hasherService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHash()
    {
        assertEquals(DEFAULT_HASH_FOR_EMPTY_PASSWORD, this.hasherService.hash(""));
    }
}

package com.cpe.springboot.user.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.cpe.springboot.user.model.User;
import com.cpe.springboot.user.repository.UserRepository;
import com.fasterxml.jackson.databind.util.ArrayIterator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private HasherService hasherService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetUser()
    {
        User expectedUser = new User("email", "firstname", "lastname", "password");
        expectedUser.setId(1);
        when(this.userRepository.findOne(anyInt())).thenReturn(expectedUser);

        User result = this.userService.getUser("1");

        assertEquals(expectedUser, result);
    }

    @Test
    public void testGetAllUsers()
    {
        User[] userArray = {
                new User("email_1", "firstname_1", "lastname_1", "password_1"),
                new User("email_2", "firstname_2", "lastname_2", "password_2")
        };

        // Mock findAll()
        Iterable<User> userIterable = new ArrayIterator<>(userArray);
        when(this.userRepository.findAll()).thenReturn(userIterable);

        // Mock expected user list
        Iterable<User> userIterableCopy = new ArrayIterator<>(userArray);
        List<User> usersExpected = new ArrayList<>();
        userIterableCopy.forEach(usersExpected::add);

        // Get all users
        List<User> users = this.userService.getAllUsers();

        assertEquals(usersExpected, users);
    }

    @Test
    public void testAddUser()
    {
        User user = new User("email_1", "firstname_1", "lastname_1", "password_1");

        assertTrue(this.userService.addUser(user));
    }
}

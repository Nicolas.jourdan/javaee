package com.cpe.springboot.user.repository;

import static org.junit.Assert.assertEquals;

import com.cpe.springboot.user.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll()
    {
        List<User> userList = new ArrayList<>();
        userList.add(new User("email_1", "firstname_1", "lastname_1", "password_1"));
        userList.add(new User("email_2", "firstname_2", "lastname_2", "password_2"));

        this.userRepository.save(userList.get(0));
        this.userRepository.save(userList.get(1));

        Iterable<User> iterableExpected = new ArrayList<User>(userList);

        assertEquals(iterableExpected, this.userRepository.findAll());
    }
}

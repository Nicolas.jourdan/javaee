package com.cpe.springboot.proxy.user.service;

import com.cpe.springboot.proxy.RequestAdapter;
import com.cpe.springboot.proxy.user.model.UserDto;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private static final String LOCALHOST = "http://user";
    private static final int USER_SERVICE_PORT = 80;
    private static final String USER_URI = "/users";

    /**
     * Send request on User service to create a user
     *
     * @param user User data to create
     *
     * @return UserService's response
     */
    public ResponseEntity addUser(UserDto user) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<UserDto> requestHeaders = new HttpEntity<UserDto>(
                user,
                headers
        );

        return RequestAdapter.sendPostRequest(
                LOCALHOST,
                USER_SERVICE_PORT,
                USER_URI,
                requestHeaders,
                UserDto.class
        );
    }

    /**
     * Send request on User service to get a user
     *
     * @param id User id
     *
     * @return UserService's response
     */
    public ResponseEntity getUser(String id) {
        return RequestAdapter.sendGetRequest(
                LOCALHOST,
                USER_SERVICE_PORT,
                USER_URI + "/" + id,
                UserDto.class
        );
    }
}

package com.cpe.springboot.proxy.authentication.service;

import com.cpe.springboot.proxy.RequestAdapter;
import com.cpe.springboot.proxy.authentication.model.CookieDto;
import com.cpe.springboot.proxy.authentication.model.LoginDto;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {
    private static final String LOCALHOST = "http://auth";
    private static final int AUTHENTICATION_SERVICE_PORT = 80;
    private static final String AUTHENTICATION_LOGIN = "/login";
    private static final String AUTHENTICATION_CHECK = "/checkAuthentication";

    /**
     * Send request on AuthenticationService to log the user
     *
     * @param loginDto Login data
     *
     * @return AuthenticationService's response
     */
    public ResponseEntity login(LoginDto loginDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<LoginDto> requestHeaders = new HttpEntity<LoginDto>(
                loginDto,
                headers
        );

        return RequestAdapter.sendPostRequest(
                LOCALHOST,
                AUTHENTICATION_SERVICE_PORT,
                AUTHENTICATION_LOGIN,
                requestHeaders,
                CookieDto.class
        );
    }

    /**
     * Send request on AuthenticationService to check the user authentication
     *
     * @param cookieDto User's cookies
     *
     * @return AuthenticationService's response
     */
    public ResponseEntity checkAuthentication(CookieDto cookieDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CookieDto> requestHeaders = new HttpEntity<CookieDto>(
                cookieDto,
                headers
        );

        return RequestAdapter.sendPostRequest(
                LOCALHOST,
                AUTHENTICATION_SERVICE_PORT,
                AUTHENTICATION_CHECK,
                requestHeaders,
                null
        );
    }
}

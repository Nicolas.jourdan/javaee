package com.cpe.springboot.proxy.authentication.controller;

import com.cpe.springboot.proxy.authentication.model.CookieDto;
import com.cpe.springboot.proxy.authentication.model.LoginDto;
import com.cpe.springboot.proxy.authentication.service.AuthenticationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProxyAuthenticationController {

    private static final Logger logger = Logger.getLogger(ProxyAuthenticationController.class);

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginDto loginDto) {
        logger.info("POST on /login");

        return this.authenticationService.login(loginDto);
    }

    @PostMapping("/checkAuthentication")
    public ResponseEntity checkAuthentication(@RequestBody CookieDto cookieDto) {
        logger.info("POST on /checkAuthentication");

        return this.authenticationService.checkAuthentication(cookieDto);
    }
}

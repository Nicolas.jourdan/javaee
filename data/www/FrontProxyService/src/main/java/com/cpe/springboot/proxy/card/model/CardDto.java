package com.cpe.springboot.proxy.card.model;

public class CardDto {

    private int id;

    private String name;

    private String description;

    private String family;

    private String hp;

    private String attack;

    private String defence;

    private String energy;

    private String imgurl;

    private int price;

    public CardDto() {
    }

    public CardDto(String name, String description, String family, String hp, String attack, String defence, String energy, String imgurl, int price) {
        this.name = name;
        this.description = description;
        this.family = family;
        this.hp = hp;
        this.attack = attack;
        this.defence = defence;
        this.energy = energy;
        this.imgurl = imgurl;
        this.price = price;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }

    public String getDefence() {
        return defence;
    }

    public void setDefence(String defence) {
        this.defence = defence;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }
}

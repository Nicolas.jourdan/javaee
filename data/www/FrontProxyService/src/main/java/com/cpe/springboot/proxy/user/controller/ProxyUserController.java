package com.cpe.springboot.proxy.user.controller;

import com.cpe.springboot.proxy.authentication.controller.ProxyAuthenticationController;
import com.cpe.springboot.proxy.user.model.UserDto;
import com.cpe.springboot.proxy.user.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProxyUserController {

    private static final Logger logger = Logger.getLogger(ProxyAuthenticationController.class);

    @Autowired
    private UserService userService;

    @PostMapping("/users")
    public ResponseEntity addUser(@RequestBody UserDto user) {
        logger.info("POST on /users");

        return this.userService.addUser(user);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity getUser(@PathVariable String id) {
        logger.info("GET on /users/" + id);

        return this.userService.getUser(id);
    }
}

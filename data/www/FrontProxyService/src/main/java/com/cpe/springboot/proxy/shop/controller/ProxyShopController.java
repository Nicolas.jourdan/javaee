package com.cpe.springboot.proxy.shop.controller;

import com.cpe.springboot.proxy.authentication.controller.ProxyAuthenticationController;
import com.cpe.springboot.proxy.shop.model.ShopDto;
import com.cpe.springboot.proxy.shop.service.ShopService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProxyShopController {

    private static final Logger logger = Logger.getLogger(ProxyAuthenticationController.class);

    @Autowired
    private ShopService shopService;

    @PostMapping("/shop/buy")
    public ResponseEntity buyCard(@RequestBody ShopDto shopDto) {
        logger.info("POST on /shop/buy");

        return this.shopService.buyCard(shopDto);
    }

    @PostMapping("/shop/sell")
    public ResponseEntity sellCard(@RequestBody ShopDto shopDto) {
        logger.info("POST on /shop/sell");

        return this.shopService.sellCard(shopDto);
    }

}

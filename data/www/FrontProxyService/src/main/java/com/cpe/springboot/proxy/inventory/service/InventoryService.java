package com.cpe.springboot.proxy.inventory.service;

import com.cpe.springboot.proxy.RequestAdapter;
import com.cpe.springboot.proxy.inventory.model.CardDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class InventoryService {

    private static final String LOCALHOST = "http://inventory";
    private static final int INVENTORY_SERVICE_PORT = 80;
    private static final String INVENTORY_SERVICE_GET_USER_CARD = "/inventory/";

    public ResponseEntity<List<CardDto>> getUserCards(String id) {
        String url = LOCALHOST + ":" + INVENTORY_SERVICE_PORT + INVENTORY_SERVICE_GET_USER_CARD + id;
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CardDto>>() {}
        );
    }
}

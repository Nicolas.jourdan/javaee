package com.cpe.springboot.proxy.authentication.model;

public class CookieDto {

    private String token;

    private String  userId;

    public CookieDto() {

    }

    public CookieDto(String token, String userId) {
        this.token = token;
        this.userId = userId;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

package com.cpe.springboot.proxy.inventory.controller;

import com.cpe.springboot.proxy.authentication.controller.ProxyAuthenticationController;
import com.cpe.springboot.proxy.inventory.model.CardDto;
import com.cpe.springboot.proxy.inventory.service.InventoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProxyInventoryController {

    private static final Logger logger = Logger.getLogger(ProxyAuthenticationController.class);

    @Autowired
    private InventoryService inventoryService;

    @GetMapping("/inventory/{id}")
    public ResponseEntity<List<CardDto>> getUserCards(
            @PathVariable String id
    ) {
        logger.info("GET on /inventory/" + id);
        return this.inventoryService.getUserCards(id);
    }
}

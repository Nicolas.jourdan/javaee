package com.cpe.springboot.proxy.shop.service;

import com.cpe.springboot.proxy.RequestAdapter;
import com.cpe.springboot.proxy.shop.model.ShopDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ShopService {

    private static final String LOCALHOST = "http://shop";
    private static final int SHOP_SERVICE_PORT = 80;
    private static final String SHOP_URI = "/shop/";

    public ResponseEntity buyCard(ShopDto shopDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ShopDto> requestHeaders = new HttpEntity<ShopDto>(
                shopDto,
                headers
        );

        return RequestAdapter.sendPostRequest(
                LOCALHOST,
                SHOP_SERVICE_PORT,
                SHOP_URI + "buy",
                requestHeaders,
                null
        );
    }

    public ResponseEntity sellCard(ShopDto shopDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<ShopDto> requestHeaders = new HttpEntity<ShopDto>(
                shopDto,
                headers
        );

        return RequestAdapter.sendPostRequest(
                LOCALHOST,
                SHOP_SERVICE_PORT,
                SHOP_URI + "sell",
                requestHeaders,
                null
        );
    }
}

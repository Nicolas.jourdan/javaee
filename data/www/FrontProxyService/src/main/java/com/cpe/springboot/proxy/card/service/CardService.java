package com.cpe.springboot.proxy.card.service;

import com.cpe.springboot.proxy.RequestAdapter;
import com.cpe.springboot.proxy.card.model.CardDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CardService {

    private static final String LOCALHOST = "http://card";
    private static final int CARD_SERVICE_PORT = 80;
    private static final String CARD_URI = "/cards";

    /**
     * Send a request to the card service to get all cards
     *
     * @return CardService's response
     */
    public ResponseEntity getCards() {
        String url = LOCALHOST + ":" + CARD_SERVICE_PORT + CARD_URI;
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<CardDto>>() {}
        );
    }

    /**
     * Send a request to the card service to get a card
     *
     * @param id Card id
     *
     * @return CardService's response
     */
    public ResponseEntity getCard(String id) {
        return RequestAdapter.sendGetRequest(
                LOCALHOST,
                CARD_SERVICE_PORT,
                CARD_URI + "/" + id,
                CardDto.class
        );
    }
}

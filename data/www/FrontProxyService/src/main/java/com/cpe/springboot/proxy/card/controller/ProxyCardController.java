package com.cpe.springboot.proxy.card.controller;

import com.cpe.springboot.proxy.authentication.controller.ProxyAuthenticationController;
import com.cpe.springboot.proxy.card.service.CardService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProxyCardController {

    private static final Logger logger = Logger.getLogger(ProxyAuthenticationController.class);

    @Autowired
    private CardService cardService;

    @GetMapping("/cards")
    public ResponseEntity getCards()
    {
        logger.info("GET on /cards");

        return this.cardService.getCards();
    }

    @GetMapping("/cards/{id}")
    public ResponseEntity getCard(@PathVariable String id)
    {
        logger.info("GET on /cards/" + id);

        return this.cardService.getCard(id);
    }
}

package com.cpe.springboot.proxy;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class RequestAdapter {

    /**
     *  Send a post request
     *
     * @return Response
     */
    public static ResponseEntity sendPostRequest(
            String hostname,
            int port,
            String uri,
            HttpEntity headers,
            Class<? extends Object> responseType
    ) {
        try {
            String url = hostname + ":" + port + uri;

            RestTemplate restTemplate = new RestTemplate();

            return restTemplate.postForEntity(url, headers, responseType);
        } catch (Exception e) {
            // Error during request generation
            return new ResponseEntity(HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Send a get request
     *
     * @return Response
     */
    public static ResponseEntity sendGetRequest(
            String hostname,
            int port,
            String uri,
            Class<? extends Object> responseType
    ) {
        String url = hostname + ":" + port + uri;
        RestTemplate restTemplate = new RestTemplate();

        return restTemplate.getForEntity(url, responseType);
    }
}

$(document ).ready(function(){
    setUserInfo();
    if (document.location.pathname.localeCompare("/sendCards.html") == 0) {
        // If the current page is the send cards page
        getUserCards();
    } else {
        // If the current page is the buy cards page
        getCards();
    }
});

$('#button-send-card').click(function () {
    var cookieUserId = getCookie("__api_user_id");
    if (!cookieUserId) {
        window.location.replace(window.location.origin + '/loginForm.html');
        return;
    }

    $.ajax({
        url: "/shop/sell",
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify({
            cardId: $('#cardId').val(),
            userId: cookieUserId
        }),
        success: function (data, textStatus, xhr){
            window.location.replace(window.location.origin + '/sendCards.html');
        },
        error: function () {
            alert("Problème lors de la vente de cette carte !");
        }
    })
});

$('#button-buy-card').click(function () {
    var cookieUserId = getCookie("__api_user_id");
    if (!cookieUserId) {
        window.location.replace(window.location.origin + '/loginForm.html');
        return;
    }

    $.ajax({
        url: "/shop/buy",
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify({
            cardId: $('#cardId').val(),
            userId: cookieUserId
        }),
        success: function (data, textStatus, xhr){
            alert("Vous venez d'acheter cette carte !")
            window.location.replace(window.location.origin + '/buyCards.html');
        },
        error: function () {
            alert("Problème lors de la vente de cette carte ! Vérifiez que vous ne l'avez pas déjà !");
        }
    })
});

function fillCurrentCard(id, familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    //FILL THE CURRENT CARD
    $('#cardId').val(id);
    $('#cardFamilyNameId')[0].innerText=familyName;
    $('#cardImgId')[0].src=imgUrl;
    $('#cardNameId')[0].innerText=name;
    $('#cardDescriptionId')[0].innerText=description;
    $('#cardHPId')[0].innerText=hp+" HP";
    $('#cardEnergyId')[0].innerText=energy+" Energy";
    $('#cardAttackId')[0].innerText=attack+" Attack";
    $('#cardDefenceId')[0].innerText=defence+" Defence";
    $('#cardPriceId')[0].innerText=price+" $";
}


function addCardToList(page,id, familyName,imgUrl,name,description,hp,energy,attack,defence,price){
    content="\
    <td><img  class='ui avatar image' src='"+imgUrl+"'> <span>"+name+" </span></td> \
    <td>"+description+"</td> \
    <td>"+familyName+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td>"+price+"$</td>\
    <td>\
        <div onclick='showCard("+id+")' class='ui vertical animated button' tabindex='0'>\
            <div onclick='showCard("+id+")' class='hidden content'>"+page+"</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";
    
    $('#cardListId tr:last').after('<tr>'+content+'</tr>');
    
    
}

function getUserCards()
{
    var cookieUserId = getCookie("__api_user_id");
    if (!cookieUserId) {
        window.location.replace(window.location.origin + '/loginForm.html');
        return;
    }

    $.ajax({
			url: "/inventory/" + cookieUserId,
			type: "GET",
			success: function (data, textStatus, xhr){

			    if (data.length > 0) {
			        // Fill the selected card with the first one
                    fillCurrentCard(
                        data[0].id,
                        data[0].family,
                        data[0].imgurl,
                        data[0].name,
                        data[0].description,
                        data[0].hp,
                        data[0].energy,
                        data[0].attack,
                        data[0].defence,
                        data[0].price
                    );
                }

				$.each(data, function (key, value) {
                    addCardToList(
                    	"Sell",
                        value.id,
                        value.family,
                        value.imgurl,
                        value.name,
                        value.description,
                        value.hp,
                        value.energy,
                        value.attack,
                        value.defence,
                        value.price
                    );
                })
			},
            error: function () {
                window.location.replace(window.location.origin + '/cardHome.html');
            }
	})
}

function showCard(id) {
    var cookieUserId = getCookie("__api_user_id");
    if (!cookieUserId) {
        window.location.replace(window.location.origin + '/loginForm.html');
        return;
    }

    $.ajax({
        url: "/cards/" + id,
        type: "GET",
        success: function (data, textStatus, xhr){
            fillCurrentCard(
                data.id,
                data.family,
                data.imgurl,
                data.name,
                data.description,
                data.hp,
                data.energy,
                data.attack,
                data.defence,
                data.price
            );
        },
        error: function () {
            window.location.replace(window.location.origin + '/cardHome.html');
        }
    })
}

function getCards() {
    $.ajax({
        url: "/cards",
        type: "GET",
        success: function (data, textStatus, xhr){

            if (data.length > 0) {
                // Fill the selected card with the first one
                fillCurrentCard(
                    data[0].id,
                    data[0].family,
                    data[0].imgurl,
                    data[0].name,
                    data[0].description,
                    data[0].hp,
                    data[0].energy,
                    data[0].attack,
                    data[0].defence,
                    data[0].price
                );
            }

            $.each(data, function (key, value) {
                addCardToList(
                	"Buy",
                    value.id,
                    value.family,
                    value.imgurl,
                    value.name,
                    value.description,
                    value.hp,
                    value.energy,
                    value.attack,
                    value.defence,
                    value.price
                );
            })
        },
        error: function () {
            window.location.replace(window.location.origin + '/cardHome.html');
        }
    })
}

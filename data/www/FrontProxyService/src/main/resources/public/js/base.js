function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

function setUserInfo() {
    checkAuthentication();
    var cookieUserId = getCookie("__api_user_id");

    if (!cookieUserId) {
        window.location.replace(window.location.origin + '/loginForm.html');
        return;
    }


    $.ajax({
        url: "/users/" + cookieUserId,
        type:'GET',
        success: function(data, textStatus, xhr) {
            setParam(data);
        },
        error: function () {
            window.location.replace(window.location.origin + '/loginForm.html');
        }
    })
}

function setParam(data) {
    $('#userNameId').text(data.firstname + " " + data.lastname);
    $('#user-money-card-home').text(data.money);
}

function checkAuthentication() {
    var cookieUserId = getCookie("__api_user_id");
    var cookieToken = getCookie("__api_token");
    if (!cookieUserId || !cookieToken) {
        window.location.replace(window.location.origin + '/loginForm.html');
        return;
    }

    $.ajax({
        url: "/checkAuthentication",
        type:'POST',
        contentType: "application/json",
        data: JSON.stringify({
            token: cookieToken,
            userId: cookieUserId
        }),
        success: function(data, textStatus, xhr) {
            // Do nothing
        },
        error: function () {
            window.location.replace(window.location.origin + '/loginForm.html');
        }
    })
}

$(document ).ready(function() {
    setUserInfo();
    $("#playButtonId").click(function(){
        window.location.replace(window.location.origin + '/roomList.html');
    });
    $("#buyButtonId").click(function(){
        window.location.replace(window.location.origin + '/buyCards.html');
    });
    $("#sellButtonId").click(function(){
        window.location.replace(window.location.origin + '/sendCards.html');
    });
});

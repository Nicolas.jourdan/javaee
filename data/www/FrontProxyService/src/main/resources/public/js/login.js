$("#login_registration").submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: "/login",
        type:'POST',
        contentType: "application/json",
        data: JSON.stringify({
            email: $(this).find('input[name="email"]').val(),
            password: $(this).find('input[name="password"]').val()
        }),
        success: function(data, textStatus, xhr) {
            setCookie("__api_token", data.token, 1);
            setCookie("__api_user_id", data.userId, 1);
            window.location.replace(window.location.origin + '/cardHome.html');
        },
        error: function () {
            alert("Problème lors de la connexion !");
        }
    })
});

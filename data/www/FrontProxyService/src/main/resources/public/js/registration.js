$("#form_registration").submit(function (e) {
    e.preventDefault();
    $.ajax({
        url: "/users",
        type:'POST',
        contentType: "application/json",
        data: JSON.stringify({
            email: $(this).find('input[name="email"]').val(),
            firstname: $(this).find('input[name="firstname"]').val(),
            lastname: $(this).find('input[name="lastname"]').val(),
            password: $(this).find('input[name="password"]').val()
        }),
        success: function(data, textStatus, xhr) {
            window.location.replace(window.location.origin + '/loginForm.html');
        },
        error: function () {
            alert("Problème lors de la création !");
        }
    })
});

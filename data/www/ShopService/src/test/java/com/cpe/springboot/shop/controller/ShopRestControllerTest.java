package com.cpe.springboot.shop.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.cpe.springboot.shop.service.ShopService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ShopRestController.class, secure = false)
public class ShopRestControllerTest {

    @InjectMocks
    private ShopRestController shopRestController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ShopService shopService;


    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testBuy() throws Exception {
        when(this.shopService.buyCard(anyString(),anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/shop/buy")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"cardId\":\"1\",\"userId\":\"1\"}");

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        assertEquals(
                HttpStatus.OK.value(),
                result.getResponse().getStatus()
        );
    }
    
    @Test
    public void testSell() throws Exception {
        when(this.shopService.sellCard(anyString(),anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/shop/sell")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"cardId\":\"1\",\"userId\":\"1\"}");

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        assertEquals(
                HttpStatus.OK.value(),
                result.getResponse().getStatus()
        );
    }
}

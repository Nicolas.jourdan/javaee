package com.cpe.springboot.shop.model;

public class ShopDto {

    private String cardId;

    private String userId;

    public ShopDto() {

    }

    public ShopDto(String userId, String cardId) {
        this.setUserId(userId);
        this.setCardId(cardId);
    }

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}

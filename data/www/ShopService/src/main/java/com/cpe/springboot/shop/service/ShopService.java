package com.cpe.springboot.shop.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cpe.springboot.shop.model.CardDto;
import com.cpe.springboot.shop.model.UserDto;

import java.util.Objects;

@Service
public class ShopService {
    // User
    private static final String USER_HOSTNAME = "http://user";
    private static final int USER_SERVICE_PORT = 80;
    private static final String USER_SERVICE_GET_USER = "/users/";
    // Card
    private static final String CARD_HOSTNAME = "http://card";
    private static final int CARD_SERVICE_PORT = 80;
    private static final String CARD_SERVICE_GET_CARD = "/cards/";
    // Inventory
    private static final String INVENTORY_HOSTNAME = "http://inventory";
    private static final int INVENTORY_SERVICE_PORT = 80;
    private static final String INVENTORY_SERVICE_GET_USER_CARD = "/inventory/";
    private static final String INVENTORY_SERVICE_ADD_CARD = "/inventory/cards/";

	public ResponseEntity buyCard(String userId, String cardId) {
		// get user
		UserDto userDto;
		CardDto cardDto;

	    try {
	        userDto = this.getUser(userId);
	    } catch (Exception e) {
	        // User not found
	        return new ResponseEntity(HttpStatus.NOT_FOUND);
	    }
		// get card
	    try {
	        cardDto = this.getCard(cardId);
	    } catch (Exception e) {
	        // Card not found
	        return new ResponseEntity(HttpStatus.NOT_FOUND);
	    }

        if (Objects.isNull(cardDto) || Objects.isNull(userDto)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        if(userDto.getMoney() > cardDto.getPrice()) {

		    if (this.hasCard(cardId, userId)) {
		        return new ResponseEntity(HttpStatus.CONFLICT);
            } else {
                //buy / add card
                try {
                    this.addCard(userId, cardDto);
                    userDto.setMoney(userDto.getMoney()-cardDto.getPrice());
                    this.updateUser(userId, userDto);
                } catch (Exception e) {
                    return new ResponseEntity(HttpStatus.CONFLICT);
                }

                return new ResponseEntity(HttpStatus.OK);
            }
		}

		return new ResponseEntity(HttpStatus.CONFLICT);
	}

    /**
     * Get user card in inventory from the InventoryService
     */
    private boolean hasCard(String cardId, String userId) {
        ResponseEntity<CardDto> response;

        String url = INVENTORY_HOSTNAME + ":" + INVENTORY_SERVICE_PORT + INVENTORY_SERVICE_GET_USER_CARD + userId + "/" + cardId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            response = restTemplate.getForEntity(url, CardDto.class);
        } catch (Exception e) {
            return false;
        }

        return response.getStatusCode().is2xxSuccessful();
    }

    public ResponseEntity sellCard(String userId, String cardId) {
		// get user
        UserDto userDto;
        CardDto cardDto;

        try {
            userDto = this.getUser(userId);
        } catch (Exception e) {
            // User not found
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        // get card
        try {
            cardDto = this.getCard(cardId);
        } catch (Exception e) {
            // Card not found
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        if (Objects.isNull(cardDto) || Objects.isNull(userDto)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        if (this.hasCard(cardId, userId)) {
            try {
                // sell / delete card
                this.deleteCard(cardId, userId);
                userDto.setMoney(userDto.getMoney()+cardDto.getPrice());
                this.updateUser(userId, userDto);
            } catch (Exception e) {
                return new ResponseEntity(HttpStatus.CONFLICT);
            }

            return new ResponseEntity(HttpStatus.OK);
        }

        return new ResponseEntity(HttpStatus.CONFLICT);
    }
	
	 /**
     * Call UserService to get a User
     *
     * @param userId : User id
     *
     * @return UserDto related to the User or null if not found
     */
    private UserDto getUser(String userId) {
        ResponseEntity<UserDto> response;

        
        String url = USER_HOSTNAME + ":" + USER_SERVICE_PORT + USER_SERVICE_GET_USER + userId;

        RestTemplate restTemplate = new RestTemplate();

        response = restTemplate.getForEntity(url, UserDto.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        return null;
    }

    /**
     * Call UserService to get a Card
     *
     * @param cardId : Card id
     *
     * @return CardDto related to the Card or null if not found
     */
    private CardDto getCard(String cardId) {
        ResponseEntity<CardDto> response;

        
        String url = CARD_HOSTNAME + ":" + CARD_SERVICE_PORT + CARD_SERVICE_GET_CARD + cardId;

        RestTemplate restTemplate = new RestTemplate();

        response = restTemplate.getForEntity(url, CardDto.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        return null;
    }

    /**
     * Add a card to the user inventory
     */
    private void addCard(String userId, CardDto card) {
        String url = INVENTORY_HOSTNAME + ":" + INVENTORY_SERVICE_PORT + INVENTORY_SERVICE_ADD_CARD + userId;

        RestTemplate restTemplate = new RestTemplate();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CardDto> requestHeaders = new HttpEntity<CardDto>(
                card,
                headers
        );

        restTemplate.postForEntity(url, requestHeaders, null);
    }

    /**
     * Delete a card in the user inventory
     */
    private void deleteCard(String cardId, String userId) {
        String url = INVENTORY_HOSTNAME + ":" + INVENTORY_SERVICE_PORT + INVENTORY_SERVICE_GET_USER_CARD + userId + "/" + cardId;

        RestTemplate restTemplate = new RestTemplate();
   
        restTemplate.delete(url);
    }

    /**
     * Update user (call UserService)
     */
    private void updateUser(String userId, UserDto userDto) {
    	String url = USER_HOSTNAME + ":" + USER_SERVICE_PORT + USER_SERVICE_GET_USER + userId;

        RestTemplate restTemplate = new RestTemplate();
   
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<UserDto> requestHeaders = new HttpEntity<UserDto>(
                userDto,
                headers
        );
        restTemplate.put(url, requestHeaders);
    }
}

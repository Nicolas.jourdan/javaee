package com.cpe.springboot.shop.controller;

import com.cpe.springboot.shop.service.ShopService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.cpe.springboot.shop.model.ShopDto;

@RestController
public class ShopRestController {

    private static final Logger logger = Logger.getLogger(ShopRestController.class);

    @Autowired
    private ShopService shopService;

    @PostMapping("/shop/buy")
    public ResponseEntity buyCard(@RequestBody ShopDto shopDto) {
        logger.info("POST /shop/buy with ShopDto ( userId:" + shopDto.getUserId() + ", cardId:" + shopDto.getCardId() + ")");
        return this.shopService.buyCard(shopDto.getUserId(),shopDto.getCardId());
    }

    @PostMapping("/shop/sell")
    public ResponseEntity sellCard(@RequestBody ShopDto shopDto) {
        logger.info("POST /shop/sell with ShopDto ( userId:" + shopDto.getUserId() + ", cardId:" + shopDto.getCardId() + ")");
        return this.shopService.sellCard(shopDto.getUserId(),shopDto.getCardId());
    }
}

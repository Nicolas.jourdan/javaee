package com.cpe.springboot.room.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import com.cpe.springboot.room.model.Room;
import com.cpe.springboot.room.repository.RoomRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@DataJpaTest
public class RoomServiceTest {

    @InjectMocks
    private RoomService roomService;

    @Mock
    private RoomRepository roomRepository;

    private List<Room> rooms;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        this.rooms = new ArrayList<>();
        rooms.add(new Room(1,1,2,1));
        rooms.add(new Room(3,3,4,4));
    }

    @Test
    public void testGetRooms()
    {
        // Mock findAll()
        Iterable<Room> roomIterable = new ArrayList<>(this.rooms);
        when(this.roomRepository.findAll()).thenReturn(roomIterable);

        // Mock expected room list
        Iterable<Room> roomIterableCopy = new ArrayList<>(this.rooms);
        List<Room> expectedRooms = new ArrayList<>();
        roomIterableCopy.forEach(expectedRooms::add);

        // Get all rooms
        List<Room> cards = this.roomService.getRooms();

        assertEquals(expectedRooms, cards);
    }

    @Test
    public void testGetRoom()
    {
        when(this.roomRepository.findOne(anyInt())).thenReturn(this.rooms.get(0));

        assertEquals(this.rooms.get(0), this.roomService.getRoom("1"));
    }
}

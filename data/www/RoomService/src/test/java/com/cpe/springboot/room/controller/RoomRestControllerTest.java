package com.cpe.springboot.room.controller;

import com.cpe.springboot.room.model.Room;
import com.cpe.springboot.room.service.RoomService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RoomRestController.class, secure = false)
public class RoomRestControllerTest {

    @InjectMocks
    private RoomRestController roomRestController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RoomService roomService;

    private Room room;

    private List<Room> rooms;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        this.room = new Room(1,1,2,1);
        this.room.setId(1);

        this.rooms = new ArrayList<>();
        this.rooms.add(new Room(3,3,4,4));
        this.rooms.add(new Room(5,5,6,6));
        this.rooms.add(new Room(7,7,8,8));
        this.rooms.add(new Room(9,9,10,10));
        this.rooms.get(0).setId(1);
        this.rooms.get(1).setId(2);
        this.rooms.get(2).setId(3);
        this.rooms.get(3).setId(4);
    }

    @Test
    public void testGetRooms() throws Exception {
        when(this.roomService.getRooms()).thenReturn(Arrays.asList(this.room));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/rooms").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "[{\"id\":1,\"useridfirstplayer\":1,\"cardidfirstplayer\":1,\"useridsecondplayer\":2,\"cardidsecondplayer\":1}]",
                result.getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void testGetRoom() throws Exception {
        when(this.roomService.getRoom(anyString())).thenReturn(this.room);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/rooms/1").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "{\"id\":1,\"useridfirstplayer\":1,\"cardidfirstplayer\":1,\"useridsecondplayer\":2,\"cardidsecondplayer\":1}",
                result.getResponse().getContentAsString(),
                false
        );
    }
}

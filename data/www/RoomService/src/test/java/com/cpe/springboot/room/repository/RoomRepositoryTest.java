package com.cpe.springboot.room.repository;

import com.cpe.springboot.room.model.Room;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RoomRepositoryTest {

    @Autowired
    private RoomRepository roomRepository;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll()
    {
        List<Room> rooms = new ArrayList<>();
        rooms.add(new Room(1,1,2,1));
        rooms.add(new Room(3,3,4,4));

        this.roomRepository.save(rooms.get(0));
        this.roomRepository.save(rooms.get(1));

        Iterable<Room> iterableExpected = new ArrayList<>(rooms);

        assertEquals(iterableExpected, this.roomRepository.findAll());
    }

    @Test
    public void testFindByUseridfirstplayer()
    {
        Room room = new Room(1,1,2,1);
        this.roomRepository.save(room);

        assertEquals(room, this.roomRepository.findByUseridfirstplayer(1));
    }
}

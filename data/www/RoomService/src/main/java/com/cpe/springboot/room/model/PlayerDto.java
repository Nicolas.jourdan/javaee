package com.cpe.springboot.room.model;

public class PlayerDto {

    private String userid;

    private String cardid;

    public PlayerDto() {
    }

    public PlayerDto(String userid, String cardid) {
        this.userid = userid;
        this.cardid = cardid;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getCardid() {
        return this.cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }
}

package com.cpe.springboot.room.repository;

import com.cpe.springboot.room.model.Room;
import org.springframework.data.repository.CrudRepository;

public interface RoomRepository extends CrudRepository<Room, Integer> {
    public Room findByUseridfirstplayer(int userId);
}

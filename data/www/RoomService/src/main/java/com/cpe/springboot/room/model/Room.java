package com.cpe.springboot.room.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int useridfirstplayer;

    private int cardidfirstplayer;

    private int useridsecondplayer;

    private int cardidsecondplayer;

    public Room() {
    }

    public Room(int useridfirstplayer, int cardidfirstplayer, int useridsecondplayer, int cardidsecondplayer) {

        this.useridfirstplayer = useridfirstplayer;
        this.cardidfirstplayer = cardidfirstplayer;
        this.useridsecondplayer = useridsecondplayer;
        this.cardidsecondplayer = cardidsecondplayer;
    }

    public Room(int useridfirstplayer, int cardidfirstplayer) {
        this.useridfirstplayer = useridfirstplayer;
        this.cardidfirstplayer = cardidfirstplayer;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUseridfirstplayer() {
        return this.useridfirstplayer;
    }

    public void setUseridfirstplayer(int useridfirstplayer) {
        this.useridfirstplayer = useridfirstplayer;
    }

    public int getCardidfirstplayer() {
        return this.cardidfirstplayer;
    }

    public void setCardidfirstplayer(int cardidfirstplayer) {
        this.cardidfirstplayer = cardidfirstplayer;
    }

    public int getUseridsecondplayer() {
        return this.useridsecondplayer;
    }

    public void setUseridsecondplayer(int useridsecondplayer) {
        this.useridsecondplayer = useridsecondplayer;
    }

    public int getCardidsecondplayer() {
        return this.cardidsecondplayer;
    }

    public void setCardidsecondplayer(int cardidsecondplayer) {
        this.cardidsecondplayer = cardidsecondplayer;
    }
}

package com.cpe.springboot.room.service;

import com.cpe.springboot.room.model.CardDto;
import com.cpe.springboot.room.model.PlayerDto;
import com.cpe.springboot.room.model.Room;
import com.cpe.springboot.room.model.UserDto;
import com.cpe.springboot.room.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class RoomService {

    private static final String LOCALHOST = "http://localhost";
    private static final int USER_SERVICE_PORT = 8086;
    private static final String USER_SERVICE_GET_USER = "/users/";

    private static final int INVENTORY_SERVICE_PORT = 8088;
    private static final String INVENTORY_SERVICE_GET_CARD = "/inventory/";

    @Autowired
    private RoomRepository roomRepository;

    public Room getRoom(String id) {
        return this.roomRepository.findOne(Integer.valueOf(id));
    }

    public List<Room> getRooms() {
        List<Room> rooms = new ArrayList<>();
        this.roomRepository.findAll().forEach(rooms::add);

        return rooms;
    }

    public boolean createRoom(Room room) {
        Room roomInDatabase = this.roomRepository.findByUseridfirstplayer(room.getUseridfirstplayer());
        if (!Objects.isNull(roomInDatabase)) {
            return false;
        }

        try {
            this.getUser(Integer.toString(room.getUseridfirstplayer()));
        } catch (Exception e) {
            return false;
        }

        try {
            this.getCard(
                    Integer.toString(room.getUseridfirstplayer()),
                    Integer.toString(room.getCardidfirstplayer())
            );
        } catch (Exception e) {
            return false;
        }

        this.roomRepository.save(room);
        return true;
    }

    public boolean addToRoom(PlayerDto playerDto, Room room) {
        UserDto userDto;
        try {
            userDto = this.getUser(playerDto.getUserid());
        } catch (Exception e) {
            return false;
        }

        if (Objects.isNull(userDto)) {
            return false;
        }

        try {
            this.getCard(
                    Integer.toString(room.getUseridfirstplayer()),
                    Integer.toString(room.getCardidfirstplayer())
            );
        } catch (Exception e) {
            return false;
        }

        room.setUseridsecondplayer(Integer.parseInt(playerDto.getUserid()));
        room.setCardidsecondplayer(Integer.parseInt(playerDto.getCardid()));
        this.roomRepository.save(room);

        return true;
    }

    public void deleteRoom(Room room) {
        this.roomRepository.delete(room);
    }

    /**
     * Call UserService to get a User
     *
     * @param userId : User id
     *
     * @return UserDto related to the User or null if not found
     */
    private UserDto getUser(String userId) {
        ResponseEntity<UserDto> response;

        String url = LOCALHOST + ":" + USER_SERVICE_PORT + USER_SERVICE_GET_USER + userId;

        RestTemplate restTemplate = new RestTemplate();

        response = restTemplate.getForEntity(url, UserDto.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        return null;
    }

    /**
     * Call CardService to get a Card
     *
     * @param cardId : Card id
     *
     * @return CardDto related to the Card or null if not found
     */
    private CardDto getCard(String userId, String cardId) {
        ResponseEntity<CardDto> response;

        String url = LOCALHOST + ":" + INVENTORY_SERVICE_PORT + INVENTORY_SERVICE_GET_CARD + userId + "/" + cardId;

        RestTemplate restTemplate = new RestTemplate();

        response = restTemplate.getForEntity(url, CardDto.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }

        return null;
    }
}

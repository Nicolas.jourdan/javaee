package com.cpe.springboot.room.controller;

import com.cpe.springboot.room.model.PlayerDto;
import com.cpe.springboot.room.model.Room;
import com.cpe.springboot.room.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class RoomRestController {

    @Autowired
    private RoomService roomService;

    @GetMapping("/rooms/{id}")
    public ResponseEntity<Room> getRoom(@PathVariable String id) {
        Room room = this.roomService.getRoom(id);

        return Objects.isNull(room) ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : new ResponseEntity<>(room, HttpStatus.OK);
    }

    @GetMapping("/rooms")
    public ResponseEntity<List<Room>> getRooms()
    {
        return new ResponseEntity<>(this.roomService.getRooms(), HttpStatus.OK);
    }

    @PostMapping("/rooms")
    public ResponseEntity createRoom(@RequestBody Room room) {
        return this.roomService.createRoom(room) ?
                new ResponseEntity(room, HttpStatus.CREATED) :
                new ResponseEntity(HttpStatus.CONFLICT);
    }

    @PostMapping("/rooms/{id}")
    public ResponseEntity addToRoom(@RequestBody PlayerDto playerDto, @PathVariable String id) {
        Room room = this.roomService.getRoom(id);

        if (Objects.isNull(room)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return this.roomService.addToRoom(playerDto, room) ?
                new ResponseEntity(HttpStatus.OK) :
                new ResponseEntity(HttpStatus.CONFLICT);
    }

    @DeleteMapping("/rooms/{id}")
    public ResponseEntity deleteRoom(@PathVariable String id) {
        Room room = this.roomService.getRoom(id);

        if (Objects.isNull(room)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        this.roomService.deleteRoom(room);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}

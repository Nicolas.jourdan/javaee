package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestInventoryApplication.class, args);
	}
}

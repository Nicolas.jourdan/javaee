package com.cpe.springboot.inventory.repository;

import com.cpe.springboot.inventory.model.Inventory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InventoryRepository extends CrudRepository<Inventory, Integer> {
    public List<Inventory> findByUserid(int id);

    public List<Inventory> findByCardidAndUseridIn(int cardId, int userId);
}

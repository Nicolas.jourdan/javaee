package com.cpe.springboot.inventory.controller;


import com.cpe.springboot.inventory.model.Card;
import com.cpe.springboot.inventory.model.Inventory;
import com.cpe.springboot.inventory.service.InventoryService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class InventoryRestController {

    private static final Logger logger = Logger.getLogger(InventoryRestController.class);

    @Autowired
    private InventoryService inventoryService;

    @GetMapping("/inventory/{id}")
    public ResponseEntity<List<Card>> getUserCards(
            @PathVariable String id
    ) {
        logger.info("GET on /inventory/" + id);
        try {
            List<Card> userCard = this.inventoryService.getCardsByUserId(id);

            logger.info("FOUND");

            return new ResponseEntity<>(userCard, HttpStatus.OK);
        } catch (Exception e) {
            logger.info("NOT FOUND");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/inventory/cards/{id}")
    public ResponseEntity<Inventory> addCard(
            @PathVariable String id,
            @RequestBody Card card

    ) {
        logger.info("POST on /inventory/cards/" + id);

        if (this.inventoryService.addUserCard(card, id)) {
            logger.info("CREATED");

            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        logger.info("CONFLICT");
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @PostMapping("/inventory/{id}")
    public ResponseEntity<Inventory> addCards(
            @PathVariable String id,
            @RequestBody List<Card> cards

    ) {
        logger.info("POST on /inventory/" + id);

        if (this.inventoryService.addUserCards(cards, id)) {
            logger.info("CREATED");

            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        logger.info("CONFLICT");

        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @GetMapping("/inventory/{userid}/{cardid}")
    public ResponseEntity<Card> getUserCard(
            @PathVariable("userid") String userid,
            @PathVariable("cardid") String cardid
    ) {
        logger.info("GET on /inventory/" + userid + "/" + cardid);

        Card card = this.inventoryService.getCardByUserId(userid, cardid);
        if (Objects.isNull(card)) {
            logger.info("NOT FOUND");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        logger.info("FOUND");

        return new ResponseEntity<>(card, HttpStatus.OK);
    }

    @DeleteMapping("/inventory/{userid}/{cardid}")
    public ResponseEntity deleteUserCard(
            @PathVariable("userid") String userid,
            @PathVariable("cardid") String cardid
    ) {
        logger.info("DELETE on /inventory/" + userid + "/" + cardid);

        if (this.inventoryService.deleteCardFromInventory(userid, cardid)) {
            logger.info("DELETE inventory with user id (" + userid + ")" + " and card id (" + cardid + ")");

            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        logger.info("NOT FOUND");

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}

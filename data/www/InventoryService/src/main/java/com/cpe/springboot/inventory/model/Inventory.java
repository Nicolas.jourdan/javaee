package com.cpe.springboot.inventory.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int cardid;

    private int userid;

    private String name;

    private String description;

    private String family;

    private String hp;

    private String attack;

    private String defence;

    private String energy;

    private String imgurl;

    private int price;

    public Inventory() {
    }

    public Inventory(int cardid, int userid, String name, String description, String family, String hp, String attack, String defence, String energy, String imgurl, int price) {
        this.cardid = cardid;
        this.userid = userid;
        this.name = name;
        this.description = description;
        this.family = family;
        this.hp = hp;
        this.attack = attack;
        this.defence = defence;
        this.energy = energy;
        this.imgurl = imgurl;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }

    public String getDefence() {
        return defence;
    }

    public void setDefence(String defence) {
        this.defence = defence;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCardid() {
        return this.cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    public int getUserid() {
        return this.userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}

package com.cpe.springboot.inventory.service;

import com.cpe.springboot.inventory.model.Card;
import com.cpe.springboot.inventory.model.Inventory;
import com.cpe.springboot.inventory.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    /**
     * Get cards from the user id
     *
     * @param id : User id
     * @return User cards
     */
    public List<Card> getCardsByUserId(String id) throws Exception {
        List<Inventory> userCards = this.inventoryRepository.findByUserid(Integer.parseInt(id));
        if (Objects.isNull(userCards)) {
            throw new Exception("User not found");
        }

        List<Card> cards = new ArrayList<>();

        for (Inventory userCard : userCards) {
            if(userCard.getUserid() == Integer.parseInt(id)){
                cards.add(new Card(userCard.getCardid(), userCard.getName(), userCard.getDescription(),
                        userCard.getFamily(), userCard.getHp(), userCard.getAttack(), userCard.getDefence(), userCard.getEnergy(), userCard.getImgurl(), userCard.getPrice()));
            }
        }

        return cards;
    }

    public boolean addUserCard(Card card, String id) {
        int cardId = card.getId();
        int userId = Integer.parseInt(id);

        // Check if the user already has the card
        List<Inventory> inventoryInDatabase = this.inventoryRepository.findByCardidAndUseridIn(cardId, userId);
        for (Inventory current : inventoryInDatabase) {
            if (current.getCardid() == cardId) {
                return false;
            }
        }
        this.inventoryRepository.save(new Inventory(cardId, userId, card.getName(), card.getDescription(),
                card.getFamily(), card.getHp(), card.getAttack(), card.getDefence(), card.getEnergy(), card.getImgurl(), card.getPrice()));

        return true;
    }

    public boolean addUserCards(List<Card> cards, String id) {
        int userId = Integer.parseInt(id);

        for(Card card : cards){
            if(!this.addUserCard(card, Integer.toString(userId))){
                return false;
            }
        }

        return true;
    }

    public Card getCardByUserId(String userid, String cardid) {
        List<Inventory> userCards = this.inventoryRepository.findByUserid(Integer.parseInt(userid));
        for (Inventory userCard : userCards) {
            if ((userCard.getCardid() == Integer.parseInt(cardid)) && (userCard.getUserid() == Integer.parseInt(userid))) {
                return new Card(userCard.getCardid(), userCard.getName(), userCard.getDescription(),
                        userCard.getFamily(), userCard.getHp(), userCard.getAttack(), userCard.getDefence(), userCard.getEnergy(), userCard.getImgurl(), userCard.getPrice());
            }
        }

        // Card not found
        return null;
    }

    public boolean deleteCardFromInventory(String userid, String cardid) {
        int cardId = Integer.parseInt(cardid);
        int userId = Integer.parseInt(userid);

        List<Inventory> userCards = this.inventoryRepository.findByCardidAndUseridIn(cardId, userId);

        for (Inventory userCard : userCards) {
            if (userCard.getCardid() == Integer.parseInt(cardid)) {

                // Remove card from the user cards
                this.inventoryRepository.delete(userCard);

                return true;
            }
        }
        return false;
    }
}

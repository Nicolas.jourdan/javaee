package com.cpe.springboot.inventory.controller;

import com.cpe.springboot.inventory.model.Card;
import com.cpe.springboot.inventory.service.InventoryService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(value = InventoryRestController.class, secure = false)
public class InventoryRestControllerTest {

    @InjectMocks
    private InventoryRestController inventoryRestController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InventoryService inventoryService;

    private Card card;

    @Before
    public void setUp(){
        this.card = new Card(0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetUserCard() throws Exception{
        when(this.inventoryService.getCardByUserId("0", "0")).thenReturn(card);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/inventory/0/0").accept(MediaType.APPLICATION_JSON);

        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        JSONAssert.assertEquals(
                "{\"id\":0,\"name\":\"Noel Flantier\",\"description\":\"Photographe\",\"family\":\"Sans famille\",\"hp\":\"4000\",\"attack\":\"9001\",\"defence\":\"666\",\"energy\":\"10\",\"imgurl\":\"image\",\"price\":7500}",
                result.getResponse().getContentAsString(),
                false
        );
    }

    @Test
    public void testAddCard() throws Exception{
        when(this.inventoryService.addUserCard(any(Card.class),eq("0"))).thenReturn(true);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/inventory/cards/0")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":0,\"name\":\"Noel Flantier\",\"description\":\"Photographe\",\"family\":\"Sans famille\",\"hp\":\"4000\",\"attack\":\"9001\",\"defence\":\"666\",\"energy\":\"10\",\"imgurl\":\"image\",\"price\":7500}");


        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();

        int httpStatus = result.getResponse().getStatus();
        Assert.assertEquals(201, httpStatus);
    }

    @Test
    public void testDeleteCard() throws Exception{
        when(this.inventoryService.deleteCardFromInventory("0", "0")).thenReturn(true);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/inventory/0/0").accept(MediaType.APPLICATION_JSON);
        MvcResult result = this.mockMvc.perform(requestBuilder).andReturn();
        int httpStatus = result.getResponse().getStatus();
        Assert.assertEquals(204, httpStatus);
    }
}

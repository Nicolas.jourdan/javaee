package com.cpe.springboot.inventory.service;

import com.cpe.springboot.inventory.model.Card;
import com.cpe.springboot.inventory.model.Inventory;
import com.cpe.springboot.inventory.repository.InventoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
public class InventoryServiceTest {
    @InjectMocks
    private InventoryService inventoryService;

    @Mock
    private InventoryRepository inventoryRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddUserCard(){
        Card card = new Card(0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        String id = "0";
        assertTrue(this.inventoryService.addUserCard(card, id));
    }

    @Test
    public void testAddUserCards(){
        Card card = new Card(0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        Card card2 = new Card(1, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        List<Card> cardList = new ArrayList<>();
        cardList.add(card);
        cardList.add(card2);
        String id = "0";
        assertTrue(this.inventoryService.addUserCards(cardList, id));
    }

    @Test
    public void testGetCardByUserId(){
        Card expectedCard = new Card(0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        Inventory inventory = new Inventory(0, 0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        inventory.setId(0);
        List<Inventory> inventoryList = new ArrayList<>();
        inventoryList.add(inventory);
        when(this.inventoryRepository.findByUserid(anyInt())).thenReturn(inventoryList);

        Card result = this.inventoryService.getCardByUserId("0", "0");

        assertEquals(expectedCard.getId(), result.getId());
    }

    @Test
    public void testGetCardSByUserId() throws Exception{
        Card expectedCard = new Card(0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        Card expectedCard2 = new Card(1, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        Inventory inventory = new Inventory(0, 0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        Inventory inventory2 = new Inventory(1, 0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        inventory.setId(0);
        inventory2.setId(1);
        List<Inventory> inventoryList = new ArrayList<>();

        inventoryList.add(inventory);
        inventoryList.add(inventory2);

        when(this.inventoryRepository.findByUserid(anyInt())).thenReturn(inventoryList);

        List<Card> result = this.inventoryService.getCardsByUserId("0");

        assertEquals(expectedCard.getId(), result.get(0).getId());
        assertEquals(expectedCard2.getId(), result.get(1).getId());
    }

    @Test
    public void testDeleteCardFromInventory(){
        Inventory inventory = new Inventory(0, 0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500);
        List<Inventory> inventoryList = new ArrayList<>();
        inventoryList.add(inventory);
        inventory.setId(0);
        when(this.inventoryRepository.findByCardidAndUseridIn(anyInt(), anyInt())).thenReturn(inventoryList);

        assertTrue(this.inventoryService.deleteCardFromInventory("0", "0"));
    }
}

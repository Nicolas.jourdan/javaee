package com.cpe.springboot.inventory.repository;

import com.cpe.springboot.inventory.model.Inventory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class InventoryRepositoryTest {
    @Autowired
    private InventoryRepository inventoryRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindByUserId(){
        List<Inventory> inventoryList = new ArrayList<>();
        inventoryList.add(new Inventory(0, 0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500));
        inventoryList.add(new Inventory(1, 0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500));

        this.inventoryRepository.save(inventoryList.get(0));
        this.inventoryRepository.save(inventoryList.get(1));

        Iterable<Inventory> iterableExpected = new ArrayList<>(inventoryList);

        assertEquals(iterableExpected, this.inventoryRepository.findByUserid(0));

    }

    @Test
    public void testFindByCardidAndUseridIn(){
        List<Inventory> inventoryList = new ArrayList<>();
        inventoryList.add(new Inventory(0, 0, "Noel Flantier", "Photographe",
                "Sans famille", "4000", "9001", "666", "10", "image", 7500));

        this.inventoryRepository.save(inventoryList.get(0));

        Iterable<Inventory> iterableExpected = new ArrayList<>(inventoryList);

        assertEquals(iterableExpected, this.inventoryRepository.findByCardidAndUseridIn(0,0));

    }
}
